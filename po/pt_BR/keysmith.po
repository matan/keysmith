# Translation of keysmith.po to Brazilian Portuguese
# Copyright (C) 2020 This file is copyright:
# This file is distributed under the same license as the keysmith package.
#
# Luiz Fernando Ranghetti <elchevive@opensuse.org>, 2020, 2021, 2023.
# André Marcelo Alvarenga <alvarenga@kde.org>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: keysmith\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-02 00:51+0000\n"
"PO-Revision-Date: 2023-03-21 09:44-0300\n"
"Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>\n"
"Language-Team: Brazilian Portuguese <kde-i18n-pt_BR@kde.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.12.3\n"

#: src/app/cli.cpp:128
#, kde-format
msgctxt "@info (<uri> placeholder)"
msgid ""
"Optional account to add, formatted as otpauth:// URI (e.g. from a QR code)"
msgstr ""
"Conta opcional a adicionar, formatada como URI otpauth://  (ex.: de um "
"código QR)"

#: src/app/flows_p.cpp:105 src/app/flows_p.cpp:246
#, kde-format
msgctxt "@title:window"
msgid "Invalid account"
msgstr "Conta inválida"

#: src/app/flows_p.cpp:106
#, kde-format
msgctxt "@info:label"
msgid ""
"The account you are trying to add is invalid. You can either quit the app, "
"or continue without adding the account."
msgstr ""
"A conta que você está tentando adicionar é inválida. Você pode ou sair do "
"aplicativo ou continuar sem adicionar a conta."

#: src/app/flows_p.cpp:247
#, kde-format
msgctxt "@info:label"
msgid ""
"The account you are trying to add is invalid. Continue without adding the "
"account."
msgstr ""
"A conta que você está tentando adicionar é inválida. Continue sem adicionar "
"a conta."

#: src/contents/ui/AccountEntryViewBase.qml:35
#, kde-format
msgctxt "Confirm dialog title: %1 is the name of the account to remove"
msgid "Removing account: %1"
msgstr "Removendo conta: %1"

#: src/contents/ui/AccountEntryViewBase.qml:42
#, kde-format
msgid ""
"<p>Removing this account from Keysmith will not disable two-factor "
"authentication (2FA). Make sure you can still access your account without "
"using Keysmith before proceeding:</p><ul><li>Make sure you have another 2FA "
"app set up for your account, or:</li><li>Make sure you have recovery codes "
"for your account, or:</li><li>Disable two-factor authentication on your "
"account</li></ul>"
msgstr ""
"<p>Remover esta conta do Keysmith não desabilitará a autenticação de dois "
"fatores (2FA). Certifique-se de que você ainda pode acessar sua conta sem "
"usar o Keysmith antes de proceder:</p><ul><li>Certifique-se de que você "
"tenha outro aplicativo de autenticação de dois fatores configurado para sua "
"conta ou: </li><li>Certifique-se de que você tem códigos de recuperação para "
"sua conta ou:</li><li>Desabilite a autenticação de dois fatores em sua "
"conta</li></ul>"

#: src/contents/ui/AccountEntryViewBase.qml:49
#, kde-format
msgctxt "Button cancelling account removal"
msgid "Cancel"
msgstr "Cancelar"

#: src/contents/ui/AccountEntryViewBase.qml:62
#, kde-format
msgctxt "Button confirming account removal"
msgid "Delete Account"
msgstr "Excluir conta"

#: src/contents/ui/AccountEntryViewBase.qml:79
#, kde-format
msgctxt "Notification shown in a passive notification"
msgid "Token copied to clipboard!"
msgstr ""

#: src/contents/ui/AccountNameForm.qml:62
#, kde-format
msgctxt "@label:textbox"
msgid "Account name:"
msgstr "Nome da conta:"

#: src/contents/ui/AccountNameForm.qml:78
#, kde-format
msgctxt "@label:textbox"
msgid "Account issuer:"
msgstr "Emissor da conta:"

#: src/contents/ui/AccountsOverview.qml:21
#, kde-format
msgctxt "@title:window"
msgid "Accounts"
msgstr "Contas"

#: src/contents/ui/AccountsOverview.qml:25
#, kde-format
msgctxt "generic error shown when adding or updating an account failed"
msgid "Failed to update accounts"
msgstr "Falha ao atualizar contas"

#: src/contents/ui/AccountsOverview.qml:26
#, kde-format
msgctxt "error message shown when loading accounts from storage failed"
msgid "Some accounts failed to load."
msgstr "Algumas contas falharam ao carregar."

#: src/contents/ui/AccountsOverview.qml:101
#, kde-format
msgid "No accounts added"
msgstr "Nenhuma conta adicionada"

#: src/contents/ui/AccountsOverview.qml:106
#, kde-format
msgctxt ""
"@action:button add new account, shown instead of overview list when no "
"accounts have been added yet"
msgid "Add Account"
msgstr "Adicionar conta"

#: src/contents/ui/AccountsOverview.qml:171
#, kde-format
msgctxt "@action:button add new account, shown in toolbar"
msgid "Add"
msgstr "Adicionar"

#: src/contents/ui/AddAccount.qml:18
#, kde-format
msgctxt "@title:window"
msgid "Add new account"
msgstr "Adicionar nova conta"

#: src/contents/ui/AddAccount.qml:57
#, kde-format
msgctxt "@label:chooser"
msgid "Account type:"
msgstr "Tipo de conta:"

#: src/contents/ui/AddAccount.qml:62
#, kde-format
msgctxt "@option:radio"
msgid "Time-based OTP"
msgstr "OTP baseado no tempo"

#: src/contents/ui/AddAccount.qml:72
#, kde-format
msgctxt "@option:radio"
msgid "Hash-based OTP"
msgstr "OTP baseado em hash"

#: src/contents/ui/AddAccount.qml:82
#, kde-format
msgid "Token secret"
msgstr "Senha do token"

#: src/contents/ui/AddAccount.qml:84
#, kde-format
msgctxt "@label:textbox"
msgid "Secret key:"
msgstr "Chave secreta:"

#: src/contents/ui/AddAccount.qml:98
#, kde-format
msgctxt "Button to reveal form for configuring additional token details"
msgid "Details"
msgstr "Detalhes"

#: src/contents/ui/AddAccount.qml:125
#, kde-format
msgctxt "@action:button cancel and dismiss the add account form"
msgid "Cancel"
msgstr "Cancelar"

#: src/contents/ui/AddAccount.qml:132 src/contents/ui/ErrorPage.qml:40
#, kde-format
msgctxt "@action:button Dismiss the error page and quit Keysmtih"
msgid "Quit"
msgstr "Sair"

#: src/contents/ui/AddAccount.qml:141 src/contents/ui/RenameAccount.qml:56
#, kde-format
msgid "Add"
msgstr "Adicionar"

#: src/contents/ui/ErrorPage.qml:33
#, kde-format
msgctxt "@action:button Button to dismiss the error page"
msgid "Continue"
msgstr "Continuar"

#: src/contents/ui/HOTPAccountEntryView.qml:19
#: src/contents/ui/TOTPAccountEntryView.qml:23
#, kde-format
msgctxt "Button for removal of a single account"
msgid "Delete account"
msgstr "Remover conta"

#: src/contents/ui/HOTPDetailsForm.qml:23
#, kde-format
msgctxt "@label:textbox"
msgid "Counter:"
msgstr "Contador:"

#: src/contents/ui/HOTPDetailsForm.qml:43
#: src/contents/ui/TOTPDetailsForm.qml:59
#, kde-format
msgctxt "@label:spinbox"
msgid "Token length:"
msgstr "Duração do token:"

#: src/contents/ui/HOTPDetailsForm.qml:55
#, kde-format
msgctxt "@option:check"
msgid "Add check digit"
msgstr "Adicionar dígito de verificação"

#: src/contents/ui/HOTPDetailsForm.qml:56
#, kde-format
msgctxt "@label:check"
msgid "Checksum:"
msgstr "Soma de verificação:"

#: src/contents/ui/HOTPDetailsForm.qml:65
#, kde-format
msgctxt "@option:check"
msgid "Use custom offset"
msgstr "Usar deslocamento personalizado"

#: src/contents/ui/HOTPDetailsForm.qml:66
#, kde-format
msgctxt "@label:check"
msgid "Truncation:"
msgstr "Truncamento:"

#: src/contents/ui/HOTPDetailsForm.qml:76
#, kde-format
msgctxt "@label:spinbox"
msgid "Truncation offset:"
msgstr "Deslocamento do truncamento:"

#: src/contents/ui/RenameAccount.qml:17
#, kde-format
msgctxt "@title:window"
msgid "Rename account to add"
msgstr "Renomear conta a adicionar"

#: src/contents/ui/RenameAccount.qml:35
#, kde-format
msgctxt ""
"@info:label Keysmith received an account to add via URI on e.g. commandline "
"which is already in use"
msgid ""
"Another account with the same name already exists. Please correct the name "
"or issuer for the new account."
msgstr ""
"Outra conta com o mesmo nome já existe. Corrija o nome ou emissor para a "
"nova conta."

#: src/contents/ui/RenameAccount.qml:49
#, kde-format
msgctxt "@action:button cancel and dismiss the rename account form"
msgid "Cancel"
msgstr "Cancelar"

#: src/contents/ui/SetupPassword.qml:17 src/contents/ui/UnlockAccounts.qml:17
#, kde-format
msgctxt "@title:window"
msgid "Password"
msgstr "Senha"

#: src/contents/ui/SetupPassword.qml:34
#, kde-format
msgid "Choose a password to protect your accounts"
msgstr "Escolha uma senha para proteger suas contas"

#: src/contents/ui/SetupPassword.qml:48
#, kde-format
msgctxt "@label:textbox"
msgid "New password:"
msgstr "Nova senha:"

#: src/contents/ui/SetupPassword.qml:55
#, kde-format
msgctxt "@label:textbox"
msgid "Verify password:"
msgstr "Verificar senha:"

#: src/contents/ui/SetupPassword.qml:63
#, kde-format
msgid "Apply"
msgstr "Aplicar"

#: src/contents/ui/SetupPassword.qml:76
#, kde-format
msgid "Failed to set up your password"
msgstr "Falha ao configurar sua senha"

#: src/contents/ui/TokenEntryViewLabels.qml:40
#, kde-format
msgctxt "placeholder text if no token is available"
msgid "(refresh)"
msgstr "(atualizar)"

#: src/contents/ui/TOTPDetailsForm.qml:24
#, kde-format
msgctxt "@label:textbox"
msgid "Timer:"
msgstr "Temporizador:"

#: src/contents/ui/TOTPDetailsForm.qml:39
#, kde-format
msgctxt "@label:textbox"
msgid "Starting at:"
msgstr "Começa em:"

#: src/contents/ui/TOTPDetailsForm.qml:70
#, kde-format
msgctxt "@label:chooser"
msgid "Hash algorithm:"
msgstr "Algoritmo hash:"

#: src/contents/ui/TOTPDetailsForm.qml:75
#, kde-format
msgctxt "@option:radio"
msgid "SHA-1"
msgstr "SHA-1"

#: src/contents/ui/TOTPDetailsForm.qml:85
#, kde-format
msgctxt "@option:radio"
msgid "SHA-256"
msgstr "SHA-256"

#: src/contents/ui/TOTPDetailsForm.qml:95
#, kde-format
msgctxt "@option:radio"
msgid "SHA-512"
msgstr "SHA-512"

#: src/contents/ui/UnlockAccounts.qml:25
#, kde-format
msgid "Failed to unlock your accounts"
msgstr "Falha ao desbloquear suas contas"

#: src/contents/ui/UnlockAccounts.qml:43
#, kde-format
msgid "Please provide the password to unlock your accounts"
msgstr "Forneça a senha para desbloquear suas contas"

#: src/contents/ui/UnlockAccounts.qml:56
#, kde-format
msgctxt "@label:textbox"
msgid "Password:"
msgstr "Senha:"

#: src/contents/ui/UnlockAccounts.qml:68
#, kde-format
msgid "Unlock"
msgstr "Desbloquear"

#: src/main.cpp:69
#, kde-format
msgctxt "@title"
msgid "Keysmith"
msgstr "Keysmith"

#~ msgctxt "placeholder text if no account name is available"
#~ msgid "(untitled)"
#~ msgstr "(sem título)"
