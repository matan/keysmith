# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the keysmith package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: keysmith\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-02 00:51+0000\n"
"PO-Revision-Date: 2023-05-03 06:43+0200\n"
"Last-Translator: Temuri Doghonadze <temuri.doghonadze@gmail.com>\n"
"Language-Team: Georgian <kde-i18n-doc@kde.org>\n"
"Language: ka\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.2.2\n"

#: src/app/cli.cpp:128
#, kde-format
msgctxt "@info (<uri> placeholder)"
msgid ""
"Optional account to add, formatted as otpauth:// URI (e.g. from a QR code)"
msgstr ""
"არასავალდებულო ანგარიშის დამატება. ფორმატით otpauth:// (მაგალითად, QR "
"კოდიდან)"

#: src/app/flows_p.cpp:105 src/app/flows_p.cpp:246
#, kde-format
msgctxt "@title:window"
msgid "Invalid account"
msgstr "არასწორი ანგარიში"

#: src/app/flows_p.cpp:106
#, kde-format
msgctxt "@info:label"
msgid ""
"The account you are trying to add is invalid. You can either quit the app, "
"or continue without adding the account."
msgstr ""
"ანგარიში, რომლის დამატებასაც აპირებთ, არასწორია. შეგიძლიათ გახვიდეთ აპიდან "
"ან გააგრძელოთ ანგარიშის დამატების გარეშე."

#: src/app/flows_p.cpp:247
#, kde-format
msgctxt "@info:label"
msgid ""
"The account you are trying to add is invalid. Continue without adding the "
"account."
msgstr ""
"ანგარიში, რომლის დამატებასაც აპირებთ, არასწორია. გააგრძელეთ ანგარიშის "
"დამატების გარეშე."

#: src/contents/ui/AccountEntryViewBase.qml:35
#, kde-format
msgctxt "Confirm dialog title: %1 is the name of the account to remove"
msgid "Removing account: %1"
msgstr "ანგარიშის წაშლა: %1"

#: src/contents/ui/AccountEntryViewBase.qml:42
#, kde-format
msgid ""
"<p>Removing this account from Keysmith will not disable two-factor "
"authentication (2FA). Make sure you can still access your account without "
"using Keysmith before proceeding:</p><ul><li>Make sure you have another 2FA "
"app set up for your account, or:</li><li>Make sure you have recovery codes "
"for your account, or:</li><li>Disable two-factor authentication on your "
"account</li></ul>"
msgstr ""
"<p>ამ ანგარიშის Keysmith-დან წაშლა არ 2FA-ს არ გამორთავს. დარწმუნდით, რომ "
"ჯერ კიდევ გაქვთ წვდომა თქვენს ანგარიშთან Keysmith-ის გარეშე, სანამ "
"გააგრძელებთ:</p><ul><li>დარწმუნდით, რომ გაქვთ სხვა 2FA აპი ან:</"
"li><li>დარწმუნდით, რომ გაქვთ აღდგენის კოდების თქვენი ანგარიშისთვის, ან:</"
"li><li>გამორთეთ 2FA თქვენი ანგარიშისთვის</li></ul>"

#: src/contents/ui/AccountEntryViewBase.qml:49
#, kde-format
msgctxt "Button cancelling account removal"
msgid "Cancel"
msgstr "გაუქმება"

#: src/contents/ui/AccountEntryViewBase.qml:62
#, kde-format
msgctxt "Button confirming account removal"
msgid "Delete Account"
msgstr "ანგარიშის წაშლა"

#: src/contents/ui/AccountEntryViewBase.qml:79
#, kde-format
msgctxt "Notification shown in a passive notification"
msgid "Token copied to clipboard!"
msgstr "კოდი დაკოპირდა ბაფერში!"

#: src/contents/ui/AccountNameForm.qml:62
#, kde-format
msgctxt "@label:textbox"
msgid "Account name:"
msgstr "ანგარიშის სახელი:"

#: src/contents/ui/AccountNameForm.qml:78
#, kde-format
msgctxt "@label:textbox"
msgid "Account issuer:"
msgstr "ანგარიშის შემქმნელი:"

#: src/contents/ui/AccountsOverview.qml:21
#, kde-format
msgctxt "@title:window"
msgid "Accounts"
msgstr "ანგარიშები"

#: src/contents/ui/AccountsOverview.qml:25
#, kde-format
msgctxt "generic error shown when adding or updating an account failed"
msgid "Failed to update accounts"
msgstr "ანგარიშების განახლების შეცდომა"

#: src/contents/ui/AccountsOverview.qml:26
#, kde-format
msgctxt "error message shown when loading accounts from storage failed"
msgid "Some accounts failed to load."
msgstr "ზოგიერთი ანგარიშის ჩატვირთვის შეცდომა."

#: src/contents/ui/AccountsOverview.qml:101
#, kde-format
msgid "No accounts added"
msgstr "ანგარიშები დამატებული არაა"

#: src/contents/ui/AccountsOverview.qml:106
#, kde-format
msgctxt ""
"@action:button add new account, shown instead of overview list when no "
"accounts have been added yet"
msgid "Add Account"
msgstr "ანგარიშის დამატება"

#: src/contents/ui/AccountsOverview.qml:171
#, kde-format
msgctxt "@action:button add new account, shown in toolbar"
msgid "Add"
msgstr "დამატება"

#: src/contents/ui/AddAccount.qml:18
#, kde-format
msgctxt "@title:window"
msgid "Add new account"
msgstr "ახალი ანგარიშის დამატება"

#: src/contents/ui/AddAccount.qml:57
#, kde-format
msgctxt "@label:chooser"
msgid "Account type:"
msgstr "ანგარიშის ტიპი:"

#: src/contents/ui/AddAccount.qml:62
#, kde-format
msgctxt "@option:radio"
msgid "Time-based OTP"
msgstr "დროით-გენერირებული OTP"

#: src/contents/ui/AddAccount.qml:72
#, kde-format
msgctxt "@option:radio"
msgid "Hash-based OTP"
msgstr "ჰეშით-გენერირებული OTP"

#: src/contents/ui/AddAccount.qml:82
#, kde-format
msgid "Token secret"
msgstr "კოდის პაროლი"

#: src/contents/ui/AddAccount.qml:84
#, kde-format
msgctxt "@label:textbox"
msgid "Secret key:"
msgstr "სადუმლო გასაღებ:"

#: src/contents/ui/AddAccount.qml:98
#, kde-format
msgctxt "Button to reveal form for configuring additional token details"
msgid "Details"
msgstr "დეტალები"

#: src/contents/ui/AddAccount.qml:125
#, kde-format
msgctxt "@action:button cancel and dismiss the add account form"
msgid "Cancel"
msgstr "გაუქმება"

#: src/contents/ui/AddAccount.qml:132 src/contents/ui/ErrorPage.qml:40
#, kde-format
msgctxt "@action:button Dismiss the error page and quit Keysmtih"
msgid "Quit"
msgstr "გასვლა"

#: src/contents/ui/AddAccount.qml:141 src/contents/ui/RenameAccount.qml:56
#, kde-format
msgid "Add"
msgstr "დამატება"

#: src/contents/ui/ErrorPage.qml:33
#, kde-format
msgctxt "@action:button Button to dismiss the error page"
msgid "Continue"
msgstr "გაგრძელება"

#: src/contents/ui/HOTPAccountEntryView.qml:19
#: src/contents/ui/TOTPAccountEntryView.qml:23
#, kde-format
msgctxt "Button for removal of a single account"
msgid "Delete account"
msgstr "ანგარიშის წაშლა"

#: src/contents/ui/HOTPDetailsForm.qml:23
#, kde-format
msgctxt "@label:textbox"
msgid "Counter:"
msgstr "მთვლელი:"

#: src/contents/ui/HOTPDetailsForm.qml:43
#: src/contents/ui/TOTPDetailsForm.qml:59
#, kde-format
msgctxt "@label:spinbox"
msgid "Token length:"
msgstr "კოდის სიგრძე:"

#: src/contents/ui/HOTPDetailsForm.qml:55
#, kde-format
msgctxt "@option:check"
msgid "Add check digit"
msgstr "შემოწმების ციფრის დამატება"

#: src/contents/ui/HOTPDetailsForm.qml:56
#, kde-format
msgctxt "@label:check"
msgid "Checksum:"
msgstr "საკონტროლო ჯამი:"

#: src/contents/ui/HOTPDetailsForm.qml:65
#, kde-format
msgctxt "@option:check"
msgid "Use custom offset"
msgstr "წანაცვლების ხელით მითითება"

#: src/contents/ui/HOTPDetailsForm.qml:66
#, kde-format
msgctxt "@label:check"
msgid "Truncation:"
msgstr "წაკვეთა:"

#: src/contents/ui/HOTPDetailsForm.qml:76
#, kde-format
msgctxt "@label:spinbox"
msgid "Truncation offset:"
msgstr "წაკვეთის წანაცვლება:"

#: src/contents/ui/RenameAccount.qml:17
#, kde-format
msgctxt "@title:window"
msgid "Rename account to add"
msgstr "დასამატებლად ანგარიშს სახელი გადაარქვით"

#: src/contents/ui/RenameAccount.qml:35
#, kde-format
msgctxt ""
"@info:label Keysmith received an account to add via URI on e.g. commandline "
"which is already in use"
msgid ""
"Another account with the same name already exists. Please correct the name "
"or issuer for the new account."
msgstr ""
"სხვა ანგარიში იგივე სახელით უკვე არსებობს. ანგარიშის შესაქმნელად ჩაასწორეთ "
"სახელი ან გამომცემელი."

#: src/contents/ui/RenameAccount.qml:49
#, kde-format
msgctxt "@action:button cancel and dismiss the rename account form"
msgid "Cancel"
msgstr "გაუქმება"

#: src/contents/ui/SetupPassword.qml:17 src/contents/ui/UnlockAccounts.qml:17
#, kde-format
msgctxt "@title:window"
msgid "Password"
msgstr "პაროლი"

#: src/contents/ui/SetupPassword.qml:34
#, kde-format
msgid "Choose a password to protect your accounts"
msgstr "ანგარიშების დასაცავად შეიყვანეთ პაროლი"

#: src/contents/ui/SetupPassword.qml:48
#, kde-format
msgctxt "@label:textbox"
msgid "New password:"
msgstr "ახალი პაროლი:"

#: src/contents/ui/SetupPassword.qml:55
#, kde-format
msgctxt "@label:textbox"
msgid "Verify password:"
msgstr "პაროლის გადამოწმება:"

#: src/contents/ui/SetupPassword.qml:63
#, kde-format
msgid "Apply"
msgstr "გამოყენება"

#: src/contents/ui/SetupPassword.qml:76
#, kde-format
msgid "Failed to set up your password"
msgstr "პაროლის დაყენების შეცდომა"

#: src/contents/ui/TokenEntryViewLabels.qml:40
#, kde-format
msgctxt "placeholder text if no token is available"
msgid "(refresh)"
msgstr "(განახლება)"

#: src/contents/ui/TOTPDetailsForm.qml:24
#, kde-format
msgctxt "@label:textbox"
msgid "Timer:"
msgstr "ტაიმერი:"

#: src/contents/ui/TOTPDetailsForm.qml:39
#, kde-format
msgctxt "@label:textbox"
msgid "Starting at:"
msgstr "დაწყების დრო:"

#: src/contents/ui/TOTPDetailsForm.qml:70
#, kde-format
msgctxt "@label:chooser"
msgid "Hash algorithm:"
msgstr "ჰეშის ალგორითმი:"

#: src/contents/ui/TOTPDetailsForm.qml:75
#, kde-format
msgctxt "@option:radio"
msgid "SHA-1"
msgstr "SHA-1"

#: src/contents/ui/TOTPDetailsForm.qml:85
#, kde-format
msgctxt "@option:radio"
msgid "SHA-256"
msgstr "SHA-256"

#: src/contents/ui/TOTPDetailsForm.qml:95
#, kde-format
msgctxt "@option:radio"
msgid "SHA-512"
msgstr "SHA-512"

#: src/contents/ui/UnlockAccounts.qml:25
#, kde-format
msgid "Failed to unlock your accounts"
msgstr "თქვენი ანგარიშების განბლოკვის შეცდომა"

#: src/contents/ui/UnlockAccounts.qml:43
#, kde-format
msgid "Please provide the password to unlock your accounts"
msgstr "ანგარიშების განსაბლოკად საჭიროა პაროლის მითითება"

#: src/contents/ui/UnlockAccounts.qml:56
#, kde-format
msgctxt "@label:textbox"
msgid "Password:"
msgstr "პაროლი:"

#: src/contents/ui/UnlockAccounts.qml:68
#, kde-format
msgid "Unlock"
msgstr "განბლოკვა"

#: src/main.cpp:69
#, kde-format
msgctxt "@title"
msgid "Keysmith"
msgstr "Keysmith"
